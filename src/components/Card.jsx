import React, { Component } from 'react'

export default class card extends Component {
  render() {
    return (
      <div className='card-body'>
        <h2>OUR VALUES</h2>
        <div className="col">
          <div className="card1">
            <div className="container">
              <i className="innovative-icon"></i>
              <h3><b>INNOVATIVE</b></h3>
              <p>We are always looking for new ways to improve our products and services.</p>
            </div>
          </div>
          <div className="card2">
            <div className="container">
              <i className="loyalty-icon"></i>
              <h3><b>LOYALTY</b></h3>
              <p>We are always looking for new ways to improve our products and services.</p>
            </div>
          </div>
          <div className="card3">
            <div className="container">
              <i className="respect-icon"></i>
              <h3><b>RESPECT</b></h3>
              <p>We are always looking for new ways to improve our products and services.</p>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

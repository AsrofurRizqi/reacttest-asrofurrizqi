import React,{ Component} from 'react'
import '../assets/index.css'

export default class Footer extends Component {
  render() {
    return (<div className='footer'>
        <div className='container'>
          <div className='row'>
            <div className='copyright'>
              <h5>Copyright © 2016. PT Company</h5>
            </div>
            <div className='icons'>
              <span>
                <i className='fb-icon' aria-hidden='true' />
                <i className='twitter-icon' aria-hidden='true' />
              </span>
            </div>
          </div>
        </div>
      </div>)
  }
}

import Card from './Card';
import Header from './Header';
import Footer from './Footer';
import Form from './Form';
import Slider from './Slider';

export {Card, Header, Footer, Form, Slider}
import React, { Component } from 'react'

export default class header extends Component {
  render() {
    return (
      <div className='header-body'>
        <div className='navbar'>
          <div className='navbar-header'>
            <a className='navbar-brand' href='/'><b>Company</b></a>
          </div>
          <div className='content'>
            <div className="dropdown">
              <button className="dropbtn">ABOUT
              </button>
              <div className="dropdown-content">
                <a href="/">HISTORY</a>
                <a href="/">VISSION MISSION</a>
              </div>
            </div>
            <div className='work'>
              <button className='work-btn'>OUR WORK</button>
            </div>
            <div className='team'>
              <button className='team-btn'>OUR TEAM</button>
            </div>
            <div className='contact'>
                <button className='contact-btn'>CONTACT</button>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

import React, { useState } from 'react';
import image1 from '../assets/images/bg.jpg';
import image2 from '../assets/images/about-bg.jpg';
import '../assets/index.css';

const Slider = () => {
  const images = [image1, image2];
  const texts = ['THIS IS A PLACE WHERE TECHNOLOGY & CREATIVITY FUSED INTO DIGITAL CHEMISTRY', 'WE DON\'T HAVE THE BEST BUT WE HAVE THE GREATEST TEAM'];
  const [currentIndex, setCurrentIndex] = useState(0);

  const goToPrevious = () => {
    setCurrentIndex(prevIndex => (prevIndex === 0 ? images.length - 1 : prevIndex - 1));
  };

  const goToNext = () => {
    setCurrentIndex(prevIndex => (prevIndex === images.length - 1 ? 0 : prevIndex + 1));
  };

  const goToImage = (index) => {
    setCurrentIndex(index);
  };

  return (
    <div className="slider">
      <div className="image-container">
        <img src={images[currentIndex]} alt={`${currentIndex + 1}`} />
        <div className="image-text">{texts[currentIndex]}</div>
        <div className="position-circles">
        {images.map((_, index) => (
          <span
            key={index}
            className={`circle ${index === currentIndex ? 'active' : ''}`}
            onClick={() => goToImage(index)}
          ></span>
        ))}
      </div>
      </div>
      <div className="arrows">
        <button className="prev" onClick={goToPrevious}>&lt;</button>
        <button className="next" onClick={goToNext}>&gt;</button>
      </div>
    </div>
  );
};

export default Slider;


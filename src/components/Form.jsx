import React, { Component } from 'react'
import '../assets/index.css'

export default class form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      message: '',
      errors: {
        name: '',
        email: '',
        message: ''
      }
    };
  }
  
  handleSubmit = (event) => {
    event.preventDefault();
    if (this.validateForm()) {
      event.target.submit();
    
    } else {
      console.log('Form validation failed');
    }
  };

  handleChange = (event) => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  };
  

  validateForm = () => {
    const { name, email, message } = this.state;
    let errors = {};
    let isValid = true;
  
    if (!name) {
      errors.name = 'This field is required.';
      isValid = false;
    }
  
    if (!email) {
      errors.email = 'This field is required.';
      isValid = false;
    } else if (!/\S+@\S+\.\S+/.test(email)) {
      errors.email = 'Email is invalid.';
      isValid = false;
    }
  
    if (!message) {
      errors.message = 'This field is required.';
      isValid = false;
    }
  
    this.setState({ errors });
    return isValid;
  };

  render() {
    const { errors } = this.state;
  
    return (
      <div className='form-body' id='form'>
        <h2>CONTACT US</h2>
        <div className='form'>
          <div className='container'>
            <div className='row'>
              <form onSubmit={this.handleSubmit}>
                <div className='form-group'>
                  <div>
                    <label htmlFor='name'>Name</label>
                    <input type='text' className={errors.name ? 'input-error' : ''} id='name' name='name' onChange={this.handleChange} />
                    {errors.name && <div className="error">{errors.name}</div>}
                  </div>
                  <div>
                    <label htmlFor='email'>Email</label>
                    <input type='text' className={errors.email ? 'input-error' : ''} id='email' name='email' onChange={this.handleChange} />
                    {errors.email && <div className="error">{errors.email}</div>}
                  </div>
                  <div>
                    <label htmlFor='message'>Message</label>
                    <textarea type='text' className={errors.message ? 'input-error' : ''} id='message' name='message' onChange={this.handleChange} />
                    {errors.message && <div className="error">{errors.message}</div>}
                  </div>
                  <div>
                    <button type='submit' className='btn btn-default'><b>SUBMIT</b></button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }  
}

import { Home} from './pages';
import './App.css';

const App = () => {
  return (
    <div className="App">
      <Home />
    </div>
  );
}

export default App;

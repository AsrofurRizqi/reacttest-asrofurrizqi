import { Header, Slider, Card, Form, Footer } from '../components';

const Home = () => {
    return (
        <div>
        <Header />
        <Slider />
        <Card />
        <Form />
        <Footer />
        </div>
    );
    }

export default Home;